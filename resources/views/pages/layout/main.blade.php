<!DOCTYPE html>
<html>
    <head>
        <title>Peculia Family</title>
        <!--/tags -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Corporation Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
        Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript">
            addEventListener("load", function () {
                setTimeout(hideURLbar, 0);
            }, false);
    
            function hideURLbar() {
                window.scrollTo(0, 1);
            }
		</script>
		
        <!--//tags -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <link rel="icon" href="{{ asset('images/peculia.ico') }}">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        
        <!-- fontawesome icons  -->
        <link href="css/font-awesome.css" rel="stylesheet">
        
        <!-- google fonts -->
        <link href="//fonts.googleapis.com/css?family=Nunito+Sans:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,vietnamese" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
		<!-- //google fonts -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script>

        @if(Session::has('success'))
            toastr.success("{{ Session::get('success') }}")

        @endif


    </script>
		
            
	</head>
	
    
    <body>
    
    <!-- header -->
    <div class="header">
        <div class="agileits_top_menu">
            <div class="container">
            <div class="w3l_header_left">
                    <ul>
                        <li><i class="fa fa-phone" aria-hidden="true"></i>0706677294, 08079579211</li>
                        <li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:info@peculiarfamilycop.org">info@peculiarfamilycop.org</a></li>
                        <li><i class="fa fa-map-marker" aria-hidden="true"></i> Suite 004 Apple Plaza, Area A last Road <span>Nyanya Abuja.</span></li>
                    </ul>
                </div>
                <div class="w3l_header_right">
                    <div class="w3ls-social-icons text-left">
                        <a class="facebook" href="https://www.facebook.com/Peculiar-Family-Co-operative-Society-Limited-222751434983372/"><i class="fa fa-facebook"></i></a>
                        <a class="twitter" href="https://twitter.com/peculiarcop"><i class="fa fa-twitter"></i></a>
                        {{--<a class="pinterest" href="#"><i class="fa fa-pinterest-p"></i></a>
                        <a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>--}}
                        {{--<a class="login" href="#" data-toggle="modal" data-target="#myModal1">Login</a>--}}
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div class="content white agile-info">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<a class="navbar-brand" href="{{ route('home')}}">
						<img src="{{ asset('images/peculia.jpeg') }}" style="height:50px;float:left;margin-right:5px">
						<h1 style="font-size:20px;float:left;">Peculiar <br>Family</h1>
					</a>
                    </div>
                    <!--/.navbar-header-->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <nav>
                            <ul class="nav navbar-nav" style="">
                                <li><a href="{{ route('home') }}">Home</a></li>
								<li><a href="{{ route('about') }}">About</a></li>
								<li><a href="{{ route('packages') }}">Our Programmes</a></li>
								<li><a href="{{ route('membershipform') }}">Membership Form</a>
                                {{--<li><a href="{{ route('benefits') }}">Benefits</a></li>
                                
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Benefits<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
										<li><a href="{{ route('faq') }}">FAQ</a></li>
										<li><a href="{{ route('benefits') }}">Membership Benefits</a></li>
										   
                                    </ul>
								</li>

								{{--<li><a href="gallery.html">Gallery</a></li>--}}
                                <li><a href="{{ route('contact') }}">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                    <!--/.navbar-collapse-->
                    <!--/.navbar-->
                </div>
            </nav>
        </div>
    </div>
    <!-- //header -->
    

    @yield('content')


	<!-- footer -->
	<div class="footer_top_agileits">
			<div class="container-fluid">
				<div class="col-md-4 footer_grid" style="">
					<h3>About Us</h3>
					<p>
						The Peculiar Family Co-operative society has a vision and drive to see that the economic wellbeing of individuals and families are improved on a daily basis.

					</p>
						<div class="read">
							<a href="#" data-toggle="modal" data-target="#myModal">Read more »</a>
						</div>
				</div>
		
				<div class="col-md-4 footer_grid">
					
					
				</div>
				
				<div class="col-md-4 footer_grid" style="float:right">
					<h3>Contact Info</h3>
					<ul class="address">
						<li><i class="fa fa-map-marker" aria-hidden="true"></i>Suite 004 Apple Plaza, Area A last Road <span>Nyanya Abuja.</span></li>
						<li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:info@example.com">info@peculiarfamilycop.org</a></li>
						<li><i class="fa fa-phone" aria-hidden="true"></i>+0123 2279 3241</li>
						<li><i class="fa fa-globe" aria-hidden="true"></i><a href="#">www.peculiarfamilycop.org</a></li>
					</ul>
				</div>
				
				<div class="clearfix"> </div>
			</div>
		</div>
	<!-- //footer -->
	
	<!-- signin Model -->
	<!-- Modal1 -->
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body modal-body-sub_agile">
					<div class="main-mailposi">
						<span class="fa fa-envelope-o" aria-hidden="true"></span>
					</div>
					<div class="modal_body_left modal_body_left1">
						<h3 class="agileinfo_sign">Login To Your Account</h3>
						<form action="#" method="post">
							<div class="styled-input agile-styled-input-top">
								<input type="text" placeholder="User Name" name="Name" required="">
							</div>
							<div class="styled-input">
								<input type="password" placeholder="Password" name="password" required="">
							</div>
							<input type="submit" value="Login">
						</form>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- //Modal content-->
		</div>
	</div>
	<!-- //Modal1 -->
	<!-- //signin Model -->
		
	<!-- bootstrap-modal-pop-up -->
	<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" style="color:#464895">
					Who We Are
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
				<div class="modal-body">
					<div class="text-center">
							<img src="{{ asset('images/peculia.jpeg') }}" alt="" class="img-responsive" height="150px" width="200px" />
					</div>
					<p>
						The Peculiar Family Co-operative society Limited was a vision concealed and birthed from the heart of a philanthropist and a man with great drive to see that the economic wellbeing of individuals and families is improved on daily bases. This was the driving force behind the registration of the Peculiar Family Co-operative society Limited on the 10TH of February, 2009 with the registration number 12602, in accordance with section 7 CAP 26 of the Northern Nigeria Co-operative Society Law. 
							The pioneer of the Peculiar Family Co-operative society Limited is Rev. Ezekiel Nyakpan who has been the President of The Peculiar Family Co-operative society Limited from 2009 till date.<br>
							Rev. Ezekiel Nyakpan has been in ministry and humanitarian service for 34 years and he is best known for humility, integrity, genuine love for humanity and helping the poor amongst us.
							His drive for the wellbeing of the common man in the society led him to partner with other like minds and patriotic citizens of Nigeria to see how jobs can be created and also to raise entrepreneurs who wouldn’t have to wait for white collar job and food security.
							Some programs were designed by the Peculiar Family Co-operative society so that registered members of the society and those anticipating to be members in the future can benefit from the program to enable them stop dependence on government for white-collar jobs but rather learn to discover themselves, their skills, God-given talents, inbuilt abilities and other gifts they were created with, so as to become entrepreneurs and employers.
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- //bootstrap-modal-pop-up --> 
		
		
	<script type="text/javascript" src="{{ asset('js/jquery-2.1.4.min.js')}}"></script>
	
		<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
		
		<!-- stats -->
		<script src="js/jquery.waypoints.min.js"></script>
		<script src="js/jquery.countup.js"></script>
		<script>
			$('.counter').countUp();
		</script>
		<!-- //stats -->
	
		<!-- owl carousel -->
		<script src="js/owl.carousel.js"></script>
		<script>
			$(document).ready(function () {
				$("#owl-demo").owlCarousel({
	
					autoPlay: 3000, //Set AutoPlay to 3 seconds
					autoPlay: true,
					items: 3,
					itemsDesktop: [991, 2],
					itemsDesktopSmall: [414, 4]
	
				});
			}); 
		</script>
		<!-- //owl carousel -->
		
		<!-- Responsive slider  -->
		<script src="js/responsiveslides.min.js"></script>
		<script>
			$(function () {
				$("#slider4").responsiveSlides({
					auto: true,
					pager: true,
					nav: true,
					speed: 1000,
					namespace: "callbacks",
					before: function () {
						$('.events').append("<li>before event fired.</li>");
					},
					after: function () {
						$('.events').append("<li>after event fired.</li>");
					}
				});
			});
		</script>
		<!-- //Responsive slider  -->
	
		<!-- Flex slider-script -->
		<script defer src="js/jquery.flexslider.js"></script>
			<script type="text/javascript">
			
			$(window).load(function(){
			  $('.flexslider').flexslider({
				animation: "slide",
				start: function(slider){
				  $('body').removeClass('loading');
				}
			  });
			});
		  </script>
		<!-- //Flex slider-script -->
		
		<!-- start-smooth-scrolling -->
		<script src="js/move-top.js"></script>
		<script src="js/easing.js"></script>
		<script>
			jQuery(document).ready(function ($) {
				$(".scroll").click(function (event) {
					event.preventDefault();
	
					$('html,body').animate({
						scrollTop: $(this.hash).offset().top
					}, 1000);
				});
			});
		</script>
		<!-- //end-smooth-scrolling -->
		
		<!-- smooth-scrolling-of-move-up -->
		<script>
			$(document).ready(function () {
				/*
				var defaults = {
					containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
				};
				*/
	
				$().UItoTop({
					easingType: 'easeOutQuart'
				});
	
			});
		</script>
		<!-- //smooth-scrolling-of-move-up -->
		
		<!-- smooth scrolling js -->
		<script src="js/SmoothScroll.min.js"></script>
		<!-- //smooth scrolling js -->
		

	</body>
	
	</html>