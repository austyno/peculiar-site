@extends('pages.layout.main')

@section('content')
@include('pages.includes.modal')

<div class="banner">
	<h2>Programmes</h2>
	<p><a href="{{ route('home') }}">Home »</a> Programmes </p>
</div>

<div class="services">
	<div class="container">
		<h3 class="heading">Programmes</h3>
			<div class="service_grids">
                
                
                @foreach($service as $s)

					@if($s->id !=5)
						<div class="col-md-3 service_grid1">
							<img src="{{ asset($s->image) }}" alt="service1" height="186px" />
						</div>
						<div class="col-md-3 service_grid2">
							<h3>{{ $s->title }}</h3>
							<p>{{substr($s->description, 0, 150)}}</p>
							<div class="read">
								<a href="#" class="details" data-toggle="modal" data-target="#myModal" data-url="{{ route('ajax',['id'=>$s->id]) }}">Read more »</a>
							</div>
						</div>
						
					@endif


				@endforeach
				


				<div class="clearfix"></div>
			</div>
	</div>

	<div class="row">
		<div class="pricing">
			@foreach($service as $serv)
				@if($serv->id == 5)

				<h3 class="heading" style="color:white">{{ $serv->title }}</h3>
				<div class="container" id="bottom">
					<p style="color:whitesmoke"> {{ substr($serv->description,0,385) }}</p>
						<div class="read">
							<button class="btn btn-sm btn-default details" style="" data-toggle="modal" data-target="#myModal" data-url="{{ route('ajax',['id'=> $serv->id])}}">Read More »</button>
						</div>
				</div>


				@endif
			@endforeach
		</div>
	</div>
</div>





@endsection
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script>
	$(document).ready(function(){
		$(document).on('click','.details', function(e){
	
			e.preventDefault();
	
			var url = $(this).data('url');
			$('#modal-loader').show();
	
			$.ajax({
				url : url,
				type : 'GET',
				dataType : 'html'
			})
			.done(function(data){
				data = JSON.parse(data);
				console.log(data);
				$('#dynamicImage').html("<img class='img-responsive' src='../../" + data.image + "' style='height:300px;width:350;margin:auto'>");
				$('#dynamicName').html(data.title);
				$('#dynamicBio').html(data.description);
				$('#modal-loader').hide();
			})
			.fail(function(){
				$('#dynamic').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				$('#modal-loader').hide();
			});
	
		});
	});
</script>