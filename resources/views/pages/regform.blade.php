@extends('pages.layout.main')

@section('content')


<div class="banner">
        <h2>Membership Form</h2>
        <p><a href="{{ route('home') }}">Home »</a> Membership Form </p>
    </div>

    <div class="faq-content" style="margin-top:0px">
        <div class="row">
                <div class="col-md-6 col-md-offset-3" style="border:.5px solid white;color:white">
                        <h2 align="center" style="">Peculiar Family Co-operative Society Ltd.</h2>
                            <h3 class="text-center">Registration Form</h3>
                            

                            @include('pages.includes.errors')
                            <span style="float:right"><strong style="color:red">*</strong> indicates required</span><br>
                                <span style="float:right">Form Number : {{$formid}}</span>
                            <form class="form-horizontal" role="form" action="{{ route('register') }}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}

                                <div class="form-group">
                                        <label for="Full name" class="col-sm-3 control-label"><span style="color:red">*</span> Full Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="name"  value="{{ old('name') }}" placeholder="Enter your full Name eg John Doe" required>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <label for="Address" class="col-sm-3 control-label"><span style="color:red">*</span> Your Address</label>
                                        <div class="col-sm-9">
                                           <input type="text" class="form-control" name="address"  value="{{ old('address') }}" placeholder="Enter Your Address" required>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <label for="Age" class="col-sm-3 control-label"><span style="color:red">*</span> Your age</label>
                                        <div class="col-sm-9">
                                           <input type="text" class="form-control" name="age"  value="{{ old('age') }}" placeholder="Enter Your Age" required>
                                           <small style="color:red">Members must be 18 years and above</small>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <label for="Project" class="col-sm-3 control-label"><span style="color:red">*</span>Business/Proposed Project</label>
                                        <div class="col-sm-9">
                                           <input type="text" class="form-control" name="project"  value="{{ old('project') }}" placeholder="Enter Your Project" required>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label for="Amount" class="col-sm-3 control-label"><span style="color:red">*</span> Amount Required</label>
                                        <div class="col-sm-9">
                                           <input type="text" class="form-control" name="amount"  value="{{ old('amount') }}" placeholder="Enter Amount required" required>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label for="state" class="col-sm-3 control-label"><span style="color:red">*</span> Your state of Origin</label>
                                        <div class="col-sm-9">
                                           <input type="text" class="form-control" name="state"  value="{{ old('state') }}" placeholder="Enter Your State" required>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label for="lga" class="col-sm-3 control-label"><span style="color:red">*</span> Local Goverment Area</label>
                                        <div class="col-sm-9">
                                           <input type="text" class="form-control" name="lga"  value="{{ old('lga') }}" placeholder="Enter Your Local Goverment Area" required>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label for="nextOfKin" class="col-sm-3 control-label"><span style="color:red">*</span> Your Next Of Kin</label>
                                        <div class="col-sm-9">
                                           <input type="text" class="form-control" name="kin"  value="{{ old('kin') }}" placeholder="Enter The name of your next of kin" required>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <label for="nextOfKinTel" class="col-sm-3 control-label"><span style="color:red">*</span> Your Next Of Kin Telephone</label>
                                        <div class="col-sm-9">
                                           <input type="text" class="form-control" name="kin_num"  value="{{ old('kin_num') }}" placeholder="Enter The number of your next of kin" required>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <label for="referer" class="col-sm-3 control-label"><span style="color:red">*</span> Your Referee Name</label>
                                        <div class="col-sm-9">
                                           <input type="text" class="form-control" name="ref"  value="{{ old('ref') }}" placeholder="Enter Your Referee Name" required>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <label for="referertel" class="col-sm-3 control-label"><span style="color:red">*</span> Your Referee Telephone Number</label>
                                        <div class="col-sm-9">
                                           <input type="text" class="form-control" name="ref_tel"  value="{{ old('ref_tel') }}" placeholder="Enter Your Referee Telephone Number" required>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <label for="refereraddres" class="col-sm-3 control-label"><span style="color:red">*</span> Your Referee Address</label>
                                        <div class="col-sm-9">
                                           <input type="text" class="form-control" name="ref_address"  value="{{ old('ref_address') }}" placeholder="Enter Your Referee Address" required>
                                        </div>
                                </div>

                                <div class="form-group">
                                        <label for="Passport" class="col-sm-3 control-label"><span style="color:red">*</span> Your Passport</label>
                                        <div class="col-sm-9">
                                           <input type="file" name="passport"  value="{{ old('passport') }}" placeholder="Enter Your Referee Address" required><br><span>Image must be less than 1mb</span>
                                        </div>
                                </div>

                                    <div class="form-group">
                                       <div class="col-sm-offset-2 col-sm-10">
                                          <div class="checkbox">
                                             <label>
                                                <input type="checkbox" name="agree" required> By submitting this form I agree to be bound by the Co-operative By-laws. <br>I also affirm that all information given is true and any falsehood found nullifys my Membership.
                                             </label>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <input type="hidden" name="formid" value="{{$formid}}">
                                    
                                    <div class="form-group">
                                       <div class="col-sm-offset-2 col-sm-10">
                                          <button type="submit" class="btn btn-default">Register</button>
                                       </div>
                                    </div>
                                 </form>
                    </div>
        </div>
    </div>
@endsection