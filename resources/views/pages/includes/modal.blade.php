<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
       aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog">
          <div class="modal-content">
              
             <div class="modal-header">
                    <button type="button" class="close"
                       data-dismiss="modal" aria-hidden="true">
                                &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                            <div id="dynamicName" style="text-align:center;font-size:24px;color:#464895"></div>
                     </h4> 
                </div>
              
                   <div class="modal-body">

                        <div id="modal-loader" style="display:none; text-align: center;">
                            <img src="{{ asset('images/ajax-loader.gif') }}">
                        </div>

                        <div id="dynamic"> 
                            <div id="dynamicImage" style="text-align:center;margin-bottom:20px;"></div>
                            
                            <div id="dynamicBio" style="padding:15px"></div>
                        </div>


                   </div>
              
                   <div class="modal-footer">
                      <button type="button" class="btn btn-default"
                         data-dismiss="modal">Close
                      </button>
                   </div>
                </div><!-- /.modal-content -->
          </div>   
    </div>