
<!DOCTYPE html>
<html>
<head>
	<title>Peculia Family</title>
	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content=""/>
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--//tags -->
	
	<!-- FlexSlider css -->
		<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
	<!-- //FlexSlider css -->

	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="icon" href="http://peculiarfamilycop.org/images/peculia.ico">
	
	<!-- for bootstrap carousel slider -->
	<link rel="stylesheet" href="css/owl.carousel.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/owl.theme.css" type="text/css" media="all">
	<!-- //for bootstrap carousel slider -->
	
	<!-- fontawesome icons  -->
	<link href="css/font-awesome.css" rel="stylesheet">
	
	<!-- google fonts -->
	<link href="//fonts.googleapis.com/css?family=Nunito+Sans:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,vietnamese" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<!-- //google fonts -->
</head>

<body>
	
<!-- header -->
<div class="header">
	<div class="agileits_top_menu" >
		<div class="container">
			<div class="w3l_header_left">
				<ul>
					<li><i class="fa fa-phone" aria-hidden="true"></i>0706677294, 08079579211</li>
					<li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:info@peculiarfamilycop.org">info@peculiarfamilycop.org</a></li>
					<li><i class="fa fa-map-marker" aria-hidden="true"></i> Suite 004 Apple Plaza, Area A last Road <span>Nyanya Abuja.</span></li>
				</ul>
			</div>
			<div class="w3l_header_right">
                    <div class="w3ls-social-icons text-left">
                        <a class="facebook" href="https://www.facebook.com/Peculiar-Family-Co-operative-Society-Limited-222751434983372/"><i class="fa fa-facebook"></i></a>
                        <a class="twitter" href="https://twitter.com/peculiarcop"><i class="fa fa-twitter"></i></a>
                        {{--<a class="pinterest" href="#"><i class="fa fa-pinterest-p"></i></a>
                        <a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>--}}
                        {{--<a class="login" href="#" data-toggle="modal" data-target="#myModal1">Login</a>--}}
                    </div>
                </div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<div class="content white agile-info">
		<nav class="navbar navbar-default fixed" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
					<a class="navbar-brand" href="{{ route('home')}}">
						<img src="{{ asset('images/peculia.jpeg') }}" style="height:50px;float:left;margin-right:5px;">
						<h1 style="font-size:20px;float:left">Peculiar <br>Family</h1>
					</a>
				</div>
				<!--/.navbar-header-->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <nav>
                            <ul class="nav navbar-nav" style="">
                                <li><a href="{{ route('home') }}">Home</a></li>
								<li><a href="{{ route('about') }}">About</a></li>
								<li><a href="{{ route('packages') }}">Our Programmes</a></li>
								<li><a href="{{ route('membershipform') }}">Membership Form</a>
                                {{--<li><a href="{{ route('benefits') }}">Benefits</a></li>
                                
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Benefits<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
										<li><a href="{{ route('faq') }}">FAQ</a></li>
										<li><a href="{{ route('benefits') }}">Membership Benefits</a></li>
										   
                                    </ul>
								</li>

								{{--<li><a href="gallery.html">Gallery</a></li>--}}
                                <li><a href="{{ route('contact') }}">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
				<!--/.navbar-collapse-->
				<!--/.navbar-->
			</div>
		</nav>
	</div>
</div>
<!-- //header -->

<!-- banner -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1" class=""></li>
		<li data-target="#myCarousel" data-slide-to="2" class=""></li>
		<li data-target="#myCarousel" data-slide-to="3" class=""></li>
		<li data-target="#myCarousel" data-slide-to="4" class=""></li>
	</ol>
	<div class="carousel-inner" role="listbox">
		<div class="item active">
			<div class="container">
				<div class="carousel-caption">
					<div class="col-md-6 banner_left">
						
						<h3 style="font-size:18px"></h3>
					</div>
					<div class="col-md-6 banner_right">
							<h3>Bottom Up Project</h3>
						<p>This empowerment program plans to reach people who have nothing they are doing.</p>
						<div class="agileits-button top_ban_agile">
							<a class="btn btn-primary btn-lg" href="{{ route('packages') }}">Read More »</a>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="item item2">
			<div class="container">
				<div class="carousel-caption text-center">
					<h3> <span>Grants.</span></h3>
					<p>In line with the vision of the Co-operative society Limited, we have been giving the go ahead by SEM (Social Exchange Market) to register members of the public and to open a model microfinance bank, in a project called “Bottom Up” project in line with the United Nations fight against abject poverty in third world nations. </p>
					
				</div>
			</div>
		</div>
		<div class="item item3">
			<div class="container">
				<div class="carousel-caption">
					<h3> <span>Wealth Management.</span></h3>
					<p>we help our registered members with professional services that combines both financial and investment advice. "Wealth that is not properly managed will soon be lost."</p>
				</div>
			</div>
		</div>
		<div class="item item4">
			<div class="container">
				<div class="carousel-caption text-right">
					<h3><span>Wealth Creation</span></h3>
					<p>we help members to create fresh ideas that can be converted into wealth, explore new financial breakthroughs, overcome their fears and expose areas of their wisdom and adventures for future life actualization. </p>
					
				</div>
			</div>
		</div>
		<div class="item item5">
			<div class="container">
				<div class="carousel-caption">
					<h3><span>Empowerment.</span></h3>
					<p>we conduct seminars and entrepreneurial trainings for the members of our co-operative society to provide them with the necessary information and direction they need to connect with their goals and make intelligent financial decisions. </p>
					
				</div>
			</div>
		</div>
	</div>
	<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		<span class="fa fa-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		<span class="fa fa-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
	<!-- The Modal -->
</div>
<!-- //banner -->


<!-- banner bottom -->
<div class="welcome">
	<div class="container">
		<div class="col-md-6 welcome_left">
			<div class="layer1">
				<div class="welcome_left_info">
					<h3>Raise your Standard of living</h3>
					<p>we provide various services to our members that help improve their standard of living. We also have programs that cover health, education, and social education.</p>
					
				</div>
			</div>
		</div>
		<div class="col-md-6 welcome_right">
			<div class="inner_grid1">
				<div class="col-md-2 icon color1">
					<span class="fa fa-file-o" aria-hidden="true"></span>
				</div>
				<div class="col-md-10 grid_info">
					<h3 style="color:white;">Wealth Management</h3>
					<p style="color:whitesmoke;font-size:20px;">we help our registered members with professional services that combines both financial and investment advice. “Wealth that is not properly managed will soon be lost</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="inner_grid1">
				<div class="col-md-2 icon color2">
					<span class="fa fa-diamond" aria-hidden="true"></span>
				</div>
				<div class="col-md-10 grid_info">
					<h3 style="color:white">Wealth Creation</h3>
					<p style="color:whitesmoke;font-size:20px;">we help members to create fresh ideas that can be converted into wealth, explore new financial breakthroughs, overcome their fears and expose areas of their wisdom and adventures for future life actualization</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="inner_grid1">
				<div class="col-md-2 icon color3">
					<span class="fa fa-line-chart" aria-hidden="true"></span>
				</div>
				<div class="col-md-10 grid_info">
					<h3 style="color:white">Empowerment</h3>
					<p style="color:whitesmoke;font-size:20px;">we conduct seminars and entrepreneurial trainings for the members of our co-operative society to provide them with the necessary information and direction they need to connect with their goals and make intelligent financial decisions.</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- //banner bottom -->

<!-- Services -->
<div class="services" id="services">
	<h3 class="heading" style="">Our Services</h3>
	<div class="container-fluid">
		<div class="services_grids">
			<div class="col-md-3 ser_grid1">
				<img src="images/agro.jpg" alt="service1" height="186px"/>
				<div class="ser_info">
					<i class="fa icon fa-suitcase"></i>
					<h4> Credits </h4>
					<i class="fa fa-plus"></i>
					<div class="clearfix"></div>
				</div>
				<p>we make arrangements for short term credits for our members in order to help them improve their agri and other business.</p>
			</div>
			<div class="col-md-3 ser_grid1">
				<img src="images/service2.jpg" alt="service1" />
				<div class="ser_info">
					<i class="fa icon fa-money"></i>
					<h4>Grants </h4>
					<i class="fa fa-plus"></i>
					<div class="clearfix"></div>
				</div>
				<p>we will help our members to access grants both localy and internationaly, for those with existing businesses and those with business ideas that lack the necessary funds to finance them.</p>
			</div>
			<div class="col-md-3 ser_grid1">
				<img src="images/esusu.png" alt="service1" height="186px" />
				<div class="ser_info">
					<i class="fa icon fa-cogs"></i>
					<h4> Saving Habits </h4>
					<i class="fa fa-plus"></i>
					<div class="clearfix"></div>
				</div>
				<p>we will act as a saving agency and encourage and teach our members to save part of their income for their future needs. Through this package they will be able to access loans and also solve their problems.</p>
			</div>
			<div class="col-md-3 ser_grid1">
				<img src="images/market1.jpg" alt="service1"  height="186px"/>
				<div class="ser_info">
					<i class="fa icon fa-users"></i>
					<h4> Standard of Living </h4>
					<i class="fa fa-plus"></i>
					<div class="clearfix"></div>
				</div>
				<p>We provide various facilities for our members that will help improve their standard of living. Our facilities will cover for health, economy, education and recreation.</p>
			</div>
			<div class="clearfix"> </div>
			<div class="ser_all">
				<a href="{{ route('packages') }}">Show all services</a>
			</div>
			
		</div>
	</div>
</div>
<!-- //Services -->

<!-- counter -->
<div class="services-bottom stats">
	<div class="banner-dott1">
		<div class="container">
			<h3 class="heading" style="font-family:cursive">Our Statistics</h3>
		  <div class="wthree-agile-counter">
		  <div class="col-md-3 w3_agile_stats_grid-top">
			<div class="w3_agile_stats_grid">
				<div class="agile_count_grid_left">
				<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
				</div>
				<div class="agile_count_grid_right">
					<p class="counter">324</p> 
				</div>
				<div class="clearfix"> </div>
				<h4>Best clients</h4>
			</div>
		</div>
		<div class="col-md-3 w3_agile_stats_grid-top">
			<div class="w3_agile_stats_grid">
				<div class="agile_count_grid_left">
					<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
				</div>
				<div class="agile_count_grid_right">
					<p class="counter">543</p> 
				</div>
				<div class="clearfix"> </div>
				<h4>Happy Customers</h4>
			</div>
		</div>
		<div class="col-md-3 w3_agile_stats_grid-top">
			<div class="w3_agile_stats_grid">
				<div class="agile_count_grid_left">
					<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
				</div>
				<div class="agile_count_grid_right">
					<p class="counter">434</p> 
				</div>
				<div class="clearfix"> </div>
				<h4>Projects Worked</h4>
			</div>
		</div>
		<div class="col-md-3 w3_agile_stats_grid-top">
			<div class="w3_agile_stats_grid">
				<div class="agile_count_grid_left">
					<span class="fa fa-trophy" aria-hidden="true"></span>
				</div>
				<div class="agile_count_grid_right">
					<p class="counter">234</p> 
				</div>
				<div class="clearfix"> </div>
				<h4>Winning Awards</h4>
			</div>
		</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	</div>
</div>
<!-- //counter -->


<!-- footer -->
<div class="footer_top_agileits">
	<div class="container-fluid">
		<div class="col-md-4 footer_grid" style="">
			<h3>About Us</h3>
			<p>
				The Peculiar Family Co-operative society has a vision and drive to see that the economic wellbeing of individuals and families are improved on a daily basis.
			</p>
				<div class="read">
					<a href="#" data-toggle="modal" data-target="#myModal">Read more »</a>
				</div>
		</div>

		<div class="col-md-4 footer_grid">
			
			
		</div>
		
		<div class="col-md-4 footer_grid" style="float:right">
			<h3>Contact Info</h3>
			<ul class="address">
				<li><i class="fa fa-map-marker" aria-hidden="true"></i>Suite 004 Apple Plaza, Area A last Road <span>Nyanya Abuja.</span></li>
				<li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:info@peculiarfamilycop.org">info@peculiarfamilycop.org</a></li>
				<li><i class="fa fa-phone" aria-hidden="true"><li>0706677294, 08079579211</li>
				<li><i class="fa fa-globe" aria-hidden="true"></i><a href="#">info@peculiarfamilycop.org</a></li>
			</ul>
		</div>
		
		<div class="clearfix"> </div>
	</div>
</div>
<div class="footer_w3ls">
	<div class="container">
		<div class="footer_bottom1">
			<p>© 2018. All rights reserved | Powered by <a href="http://appnovtech.com">AppNov Tech</a></p>
		</div>
	</div>
</div>
<!-- //footer -->

<!-- signin Model -->
<!-- Modal1 -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body modal-body-sub_agile">
				<div class="main-mailposi">
					<span class="fa fa-envelope-o" aria-hidden="true"></span>
				</div>
				<div class="modal_body_left modal_body_left1">
					<h3 class="agileinfo_sign">Login To Your Account</h3>
					<form action="#" method="post">
						<div class="styled-input agile-styled-input-top">
							<input type="text" placeholder="User Name" name="Name" required="">
						</div>
						<div class="styled-input">
							<input type="password" placeholder="Password" name="password" required="">
						</div>
						<input type="submit" value="Login">
					</form>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- //Modal content-->
	</div>
</div>
<!-- //Modal1 -->
<!-- //signin Model -->
	
<!-- bootstrap-modal-pop-up -->
	<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" style="color:#464895">
					Who We Are
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
				<div class="modal-body">
					<div class="text-center">
							<img src="{{ asset('images/peculia.jpeg') }}" alt="" class="img-responsive" height="150px" width="200px" />
					</div>
					<p>
						The Peculiar Family Co-operative society Limited was a vision concealed and birthed from the heart of a philanthropist and a man with great drive to see that the economic wellbeing of individuals and families is improved on daily bases. This was the driving force behind the registration of the Peculiar Family Co-operative society Limited on the 10TH of February, 2009 with the registration number 12602, in accordance with section 7 CAP 26 of the Northern Nigeria Co-operative Society Law. 
							The pioneer of the Peculiar Family Co-operative society Limited is Rev. Ezekiel Nyakpan who has been the President of The Peculiar Family Co-operative society Limited from 2009 till date.<br>
							Rev. Ezekiel Nyakpan has been in ministry and humanitarian service for 34 years and he is best known for humility, integrity, genuine love for humanity and helping the poor amongst us.
							His drive for the wellbeing of the common man in the society led him to partner with other like minds and patriotic citizens of Nigeria to see how jobs can be created and also to raise entrepreneurs who wouldn’t have to wait for white collar job and food security.
							Some programs were designed by the Peculiar Family Co-operative society so that registered members of the society and those anticipating to be members in the future can benefit from the program to enable them stop dependence on government for white-collar jobs but rather learn to discover themselves, their skills, God-given talents, inbuilt abilities and other gifts they were created with, so as to become entrepreneurs and employers.
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>

	<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
	
	<!-- stats -->
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.countup.js"></script>
	<script>
		$('.counter').countUp();
	</script>
	<!-- //stats -->

	<!-- owl carousel -->
	<script src="js/owl.carousel.js"></script>
	<script>
		$(document).ready(function () {
			$("#owl-demo").owlCarousel({

				autoPlay: 3000, //Set AutoPlay to 3 seconds
				autoPlay: true,
				items: 3,
				itemsDesktop: [991, 2],
				itemsDesktopSmall: [414, 4]

			});
		}); 
	</script>
	<!-- //owl carousel -->
	
	<!-- Responsive slider  -->
	<script src="js/responsiveslides.min.js"></script>
	<script>
		$(function () {
			$("#slider4").responsiveSlides({
				auto: true,
				pager: true,
				nav: true,
				speed: 1000,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});
		});
	</script>
	<!-- //Responsive slider  -->

	<!-- Flex slider-script -->
	<script defer src="js/jquery.flexslider.js"></script>
		<script type="text/javascript">
		
		$(window).load(function(){
		  $('.flexslider').flexslider({
			animation: "slide",
			start: function(slider){
			  $('body').removeClass('loading');
			}
		  });
		});
	  </script>
	<!-- //Flex slider-script -->
	
	<!-- start-smooth-scrolling -->
	<script src="js/move-top.js"></script>
	<script src="js/easing.js"></script>
	<script>
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();

				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- //end-smooth-scrolling -->
	
	<!-- smooth-scrolling-of-move-up -->
	<script>
		$(document).ready(function () {
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/

			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>
	<!-- //smooth-scrolling-of-move-up -->
	
	<!-- smooth scrolling js -->
	<script src="js/SmoothScroll.min.js"></script>
	<!-- //smooth scrolling js -->

</body>
</html>