<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[
    'uses' => 'PagesController@home',
    'as' => 'home'
]);

Route::get('/about',[
    'uses' =>'PagesController@about',
    'as' => 'about'
]);
Route::get('/benefits',[
    'uses' => 'PagesController@benefits',
    'as' => 'benefits'
]);
Route::get('/faq',[
    'uses' => 'PagesController@faq',
    'as' => 'faq'
]);
Route::get('/membershipform',[
    'uses' => 'PagesController@forms',
    'as' => 'membershipform'
]);

Route::get('/contact',[
    'uses' => 'PagesController@contact',
    'as' => 'contact'
]);

Route::get('/packages',[
    'uses' => 'PagesController@packages',
    'as' => 'packages'
]);
Route::get('/ajax/{id}',[
    'uses' => 'PagesController@ajax',
    'as' => 'ajax'
]);
Route::post('/pay',[
    'uses' => 'PaymentController@redirectToGateway',
    'as' => 'pay'
]);
Route::get('/paystackcallback',[
    'uses' => 'PaymentController@handleGatewayCallback',
    'as' => 'paystackcallback'
]);

Route::post('/register',[
    'uses' => 'PagesController@register',
    'as' => 'register'
]);










