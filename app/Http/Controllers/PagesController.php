<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use Paystack;
use Session;
use App\Registeration;
class PagesController extends Controller
{
    public function home(){
        return view('pages.home');
    }

    public function about(){
        return view('pages.about');
    }

    public function benefits(){
       
        return view('pages.benefits');
    }

    public function faq(){
        return view('pages.faq');
    }

    public function forms(){
        return view('pages.membershipform');
    }

    public function contact(){
        return view('pages.contactus');
    }

    public function packages(){
        $service = Service::all();

        return view('pages.packages')->with('service',$service);
    }

    public function ajax(Request $request, $id){
        $service = Service::find($id);
        $service = json_encode($service);

        return $service;
    }
    
     public function register(Request $request){
         
         
         //dd($request->all());

        $this->validate($request,[
            'name'=>'required|min:3|max:200',
            'address' => 'required',
            'age' => 'required|integer',
            'project' => 'required',
            'amount' => 'required|integer',
            'state' => 'required',
            'lga' => 'required',
            'kin' => 'required',
            'kin_num' => 'required|',
            'ref' => 'required',
            'ref_tel' => 'required|' ,
            'ref_address' => 'required',
            //'passport' => 'required|image|max:5000000 mb',
            'formid' => 'required'
            
        ]);
        
        
       /* $img = $request->passport;

        $imagename = $img->getClientOriginalName();
        $newimagename = time().'_'.$imagename;
        $upload =  $img->move('memberpics/',$newimagename);
        */
        
        
        
        /* $image = imagecreatefromstring(file_get_contents($tmp_dir));
        $saveLocation = “user_images/”.md5(uniqid()).”png”;

        imagepng($image,$saveLocation,9);*/

        
        $filenamewithextension = $request->file('passport')->getClientOriginalName();

        //get filename without extension
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

        //get file extension
        $extension = $request->file('passport')->getClientOriginalExtension();

        //filename to store
        $filenametostore = $filename.'_'.time().'.'.$extension;
        

        //Upload File
        
        //$request->file('passport')->storeAs(public_path('memberpics/'),$filenametostore);
        
        $request->file('passport')->move(public_path('memberpics/'),$filenametostore);
        
        
        
      /*  if(isset($_POST['passport'])){
      if (isset ($_FILES['passport'])){
          
          $imagename = $_FILES['passport']['name'];
          $source = $_FILES['passport']['tmp_name'];
          $target = "memberpics/".$imagename;
          move_uploaded_file($source, $target);

          $imagepath = $imagename;
          
          $file = "images/" . $imagepath; //This is the original file

          list($width, $height) = getimagesize($file); 

          $tn = imagecreatetruecolor($width, $height);

          //$image = imagecreatefromjpeg($file);
          $info = getimagesize($target);
          if ($info['mime'] == 'image/jpeg'){
            $image = imagecreatefromjpeg($file);
          }elseif ($info['mime'] == 'image/gif'){
            $image = imagecreatefromgif($file);
          }elseif ($info['mime'] == 'image/png'){
            $image = imagecreatefrompng($file);
          }

          imagecopyresampled($tn, $image, 0, 0, 0, 0, $width, $height, $width, $height);
          imagejpeg($tn, $save, 60);

          

         }
            
    }
       
*/
       
        
        $registered = Registeration::create([
            'name' => $request->name,
            'address' => $request->address,
            'age' => $request->age,
            'project' => $request->project,
            'amount' => $request->amount,
            'state' => $request->state,
            'lga' => $request->lga,
            'next_of_kin' => $request->kin,
            'next_of_kin_tel' => $request->kin_num,
            'referee_name' => $request->ref,
            'referee_tel' => $request->ref_tel,
            'referee_address' => $request->ref_address,
            'passport' => $filenametostore,
            'formnum' => $request->formid

        ]);

        Session::flash('success','Congrats you have successfully Registered ');
        
 return redirect()->route('membershipform');

        
    
    }
}
